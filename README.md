Datasets for Drowsy-DC evaluation
=================================

This repository is linked to the publication "Drowsy-DC: Data center power management inspired by smart phones". It contains datasets used for evaluation, as well as code to extract interesting data from the Google dataset.


## Datasets

The datasets are usage traces of virtual machines running in a data-center. They give information about the activity of the virtual machines via the measured CPU usage.

There are two datasets, separated into subfolders named:
* "google": Google cluster usage traces. It is [publicly available](https://github.com/google/cluster-data). As described in its official repository, it "represents 29 day's worth of cell information from May 2011, on a cluster of about 12.5k machines";
* "other": usage traces of 57 virtual machines from another public cloud, collected over 8 days.

## Sampling

Moreover, the folder "sampling" contains code to sample each dataset, that is to say, identify interesting traces. Its subfolder "google" contains sampling code for the Google traces, and "other" contains sampling code for the other dataset. Please have a look at the README file to learn more about the sampling technique.

A file "sample.pdf" is included to show a sample of the corresponding traces.
