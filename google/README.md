Google datasets
===============

The Google traces are huge (several hundred of Gigabytes), and are publicly available anyway. Thus, this folder only contains sampled traces. How to obtain the full traces is explained after the description of the sampled ones.

### Samples

Given the nature of the Google traces (mainly always-active, rather short taks), it requires a lot of work to extract interesting traces. An interesting trace, is one that is long enough (at least 15 days) and exhibits periodic variation in activity. Furthermore, we need to find several of such traces.

The 6 traces in this folder, are among the ones that respect the following criteria:
 * long enough (15 days);
 * periodic variation in activity;
 * matching periodic variations.

The format of the samples is the following: first column is the timestamp of the measure, in microseconds; second column is the CPU activity, and last column is the memory consumption. Please check the [Google trace documentation](https://drive.google.com/file/d/0B5g07T_gRDg9Z0lsSTEtTWtpOW8/view) to understand how these values are obtained.

### Downloading Google traces

The Google traces are actually of several types. In order to obtain exploitable traces for evaluation, we need two of them:
* task events;
* task resource usage.

(see the README in "sampling" to learn more)

Downloading the datasets is preferrably done using [gsutil](https://cloud.google.com/storage/docs/gsutil?csw=1), a command line utility by Google, to download data from the Google Storage for Developers. So first, install gsutil, which is part of the Google Cloud SDK, either by pulling the package from your distribution's repositories, or manually by following [this link](https://cloud.google.com/sdk/docs/#linux).

Now you can download both datasets using `gsutil -m cp -r gs://clusterdata-2011-2/<folder>/ .`. `-m` will ask gsutil to perform a parallel copy, while `-r` simply is the classic flag for the cp command to copy recursively. Folders names are:
* task events: "task_events";
* task resource usage: "task_usage".

Documentation for the Google datasets is available here: [Google cluster-usage traces: format + schema](https://drive.google.com/file/d/0B5g07T_gRDg9Z0lsSTEtTWtpOW8/view).
