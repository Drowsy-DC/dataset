Dataset samplers
================

This folder contains code to sample traces of each dataset, i.e. extract data useful for Drowsy-DC simulation and evaluation.

Please read the README file in each subdirectory for more details about the sampling operation specific to the traces type.

Moreover, you can find an sample illustration of each traces type as a PDF file named "sample.pdf" in each subfolder.
