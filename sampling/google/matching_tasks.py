#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""Find matching tasks in the processed Google traces.

Matching tasks are tasks that have measure records during the same interval, and
exhibit matching patterns of inactivity.

Usage:
    ./matching_tasks.py FILTERED_TASKS NB_MATCHING_TASKS NB_TRIED_TASKS
                        MATCHING_ERROR_THRESH

 * FILTERED_TASKS is the directory containing the filtered traces measures as
    output by the Hadoop job; you may wish to run missingrecords_taskfilter.py
    on them first to eliminate traces with too many bad records
 * NB_MATCHING_TASKS is the number of matching tasks to find
 * NB_TRIED_TASKS is the number of tasks that are selected among the longest ones,
    before looking for matching tasks
 * MATCHING_ERROR_THRESH is the threshold for relative matching error between
    two examined tasks (0.1, i.e. 10% error already gives good results)

First, this program selects the NB_TRIED_TASKS longest tasks among all tasks in
FILTERED_TASKS. Then, for each such parent task, it tries to find
NB_MATCHING_TASKS - 1 other tasks that have their measure records for the same
dates as the selected parent task; the other tasks shall exhibit an inactivity
pattern close enough to the one of the parent, the criterium for the relative
matching error being MATCHING_ERROR_THRESH. Finally, it scans the sets of
matching tasks that were found and determines the best matching one.
"""


import sys
import os
import glob
import gzip
import logging
from itertools import combinations
from operator import attrgetter
from math import fsum, sqrt


class Task:
    """Represents a Task in Google traces.

     * job_id: job ID of the Task (int)
     * task_id: task ID of the Task inside its job (int)
     * begin: timestamp of the first measure record for the Task (int)
     * end: timestamp of the last measure record for the Task (int)
     * length: length of the measure records period (int)
     * measures: measure records of the Task (generator or dictionary)

    There can be a huge number of measure records for a single Task. As such,
    while begin and end shall be provided on Task creation, measures can be
    either a dict, mapping records (float) to timestamps (int); or a generator
    of such tuples. In the latter case, this class will handle the lazy loading
    of measures when it is required. Moreover, because of witnessed inconsistency
    between read begin and end values, and actual values in the measures dict,
    the lazy loading will also update begin, end and length according to the
    actual indices in the measures dict.

    Note: begin, end and length are **not** updated until measures is accessed.
    If their real value is needed before measures being actually accessed, use
    the method force_load.

    The method for the `in` operator is overriden, in order to test for inclusion
    between Tasks. `task1 in task2` means that all measure records of task1 are
    after the first record of task2, and before the last record of task2, bounds
    included.
    """
    def __init__(self, job_id, task_id, begin, end, measures):
        self.job_id = job_id
        self.task_id = task_id
        self.begin = begin
        self.end = end
        self.length = end - begin
        self.measures = measures

    def force_load(self):
        """Force loading measures, and updating begin, end and length.

        See the docstring of the class for why this method exists. It can be
        necessary if the real value of begin, end or length is needed.
        """
        # Trigger the update included in __getattribute__
        if not isinstance(object.__getattribute__(self, 'measures'), dict):
            self.__getattribute__('measures')

    def __contains__(self, other):
        return other.begin >= self.begin and other.end <= self.end

    def __getattribute__(self, name):
        """Override to handle lazy loading of measures and updating related members.

        measures is lazily loaded on demand. However, begin and end, as provided
        on Task creation, may not correspond to exact dates in measures (for
        reasons... dunno why). So when measures is requested and loaded, we
        update begin, end and length accordingly.
        """
        # As per the documentation, to access an attribute, it is necessary to
        # explicitly call object.__getattribute__ in order to avoid infinite
        # recursion (I witnessed this phenomenon indeed).
        if name == 'measures' and \
                not isinstance(object.__getattribute__(self, 'measures'), dict):
            self.measures = dict(object.__getattribute__(self, 'measures'))
            self.begin = min(object.__getattribute__(self, 'measures').keys())
            self.end = max(object.__getattribute__(self, 'measures').keys())
            self.length = object.__getattribute__(self, 'end') - \
                object.__getattribute__(self, 'begin')
        return object.__getattribute__(self, name)

    def __str__(self):
        return str(self.job_id) + '_' + str(self.task_id)

class NoNestedTask(Exception):
    """Raised when no nested Task could be found.

    A nested Task, is a Task with all its measure records after the first record,
    and before the last record of the parent Task.
    """
    pass

class TaskMeasuresInconsistencyError(Exception):
    """Raised when an inconsistency is discovered in measure records dates.

    For some unknown reason, an inconsistency between begin and end timestamps
    of a Task, and actual timestamp indices in the measures of this Task can
    exist.

    This exception may be used to notify the user that such a Task exist and
    should probably be removed from the input pool.
    """
    def __init__(self, task, msg=None):
        super().__init__(self, task, msg)
        self.task = task
        self.msg = msg

    def __str__(self):
        return 'task {}: lifespan {}-{} inconsistent with value from filtered ' \
               'tasks{}'.format(self.task, self.task.begin, self.task.end,
                                '\n' + self.msg if self.msg is not None else '')

def read_traces(dirname):
    """Read task traces from the given directory.

    Returns a generator that yields Task class instances. Their measures field
    is a generator for lazy loading (related: docstring of Task class).
    """
    job_dirs = glob.glob(os.path.join(dirname, '*'))
    nb_jobs = len(job_dirs)
    progress = 0
    for job_dir in job_dirs:
        progress += 1
        logging.info('Reading tasks traces (%f)', 100 * progress / nb_jobs)
        job_id = int(os.path.basename(job_dir))
        for task_file in glob.glob(os.path.join(job_dir, '*.gz')):
            task_id = int(os.path.basename(task_file).split('-', maxsplit=1)[0])
            with gzip.open(task_file, 'r') as task_measures:
                # Force decompress the Gzip trace file
                # Was it a normal text file, we could do better to get only the
                # first and last lines, using seek. But because of the Gzip
                # compression, seeking is impossible, so let's just uncompress
                # the file...
                # I keep the lazy loading (read_measures returns a generator
                # over the file, used when actually reading the measures), I
                # guess I could keep the uncompressed file in memory if the
                # machine has enough of it (most probable). Although decompressing
                # every file almost defeats the purpose of lazy loading, it still
                # saves time because we don't parse each record line.
                records = task_measures.readlines()
                begin = int(records[1].decode('utf-8').split('\t')[0])
                end = int(records[-1].decode('utf-8').split('\t')[0])
            yield Task(job_id, task_id, begin, end,
                       read_measures(dirname, job_id, task_id))

def read_measures(dirname, job_id, task_id):
    """Read measure records of the given task.

    Return a generator of tuples (timestamp, measure) (int, float), where
    timestamp is the date of the record and measure is the recorded CPU usage.
    """
    # Measures files may be in several parts as output by Hadoop, however I did
    # not witness this behavior. At any rate, the actual file has "-r-xxx.gz"
    # appended to its name, which is not predictible, so glob is always useful.
    for measures_partfile in glob.glob(
            os.path.join(dirname, str(job_id), str(task_id) + '*')):
        with gzip.open(measures_partfile, 'r') as measures_part:
            for measure in measures_part:
                timestamp, value = measure.decode('utf-8').split('\t')[0:2]
                yield int(timestamp), float(value)

def get_matchingtasks(tasks, nb_tasks, rel_matchingerror_thresh, parent=None):
    """Find nb_tasks Tasks that match the given parent Task, among tasks.

    Return a set of Tasks.

    The next matching Task is chosen as the longest included in the parent one,
    and with a relative matching error less than rel_matchingerror_thresh.
    If parent is None, then it selects the Task with the longest length.
    """
    # We found enough matching tasks, yay! Return the empty set to end the recursion
    if nb_tasks == 0:
        return set()

    # Let's not modify the global set of tasks ;)
    cur_tasks = tasks.copy()

    # Exits when enough matching (nested) tasks were found, or raise NoNestedTask
    # for the calling function otherwise
    while True:
        while True:
            # Will raise NoNestedTask for us
            chosen = _find_biggest_nested(cur_tasks, parent)
            try:
                error = matching_error({chosen, parent})
            except TaskMeasuresInconsistencyError as err:
                logging.warning('Inconsistent lifespan for task %s', err.task)
                continue
            else:
                if error < rel_matchingerror_thresh:
                    break
            finally:
                cur_tasks -= {chosen}
        logging.debug('Need %d more matching tasks', nb_tasks - 1)
        try:
            # chosen has already been removed from cur_tasks at this point
            return {chosen} | get_matchingtasks(
                cur_tasks, nb_tasks - 1, rel_matchingerror_thresh, chosen)
        except NoNestedTask:
            # Will loop, and try with another chosen candidate until
            # _find_biggest_nested raises NoNestedTask itself
            continue

def _find_biggest_nested(tasks, parent=None):
    """Find the longest Task included in parent.

    Return the Task with the greater length, with its records included between
    the begin and end dates of the parent.
    If parent is None, return the longest Task among tasks.
    """
    # Just a dummy, empty Task
    chosen = Task(0, 0, 0, 0, {})

    for task in tasks:
        if (parent is None or (not parent is None and task in parent)) \
                and task.length > chosen.length:
            chosen = task

    # We're still with the dummy Task...
    if chosen.length == 0:
        raise NoNestedTask()

    return chosen

def _find_matchingslice(tasks):
    """Find the longest date slice that matches all tasks.

    tasks are Tasks where one is included into another one, up to the parent task.

    You may **need to force_load all tasks** before calling this function,
    depending on your usage of its return value (if you need indexing measures).

    Useful to determine the timespan on which to compute the matching error.
    """
    shortest = min(tasks, key=attrgetter('length'))

    return (shortest.begin, shortest.end)

def matching_error(tasks):
    """Compute the matching error between the tasks.

    The matching error is the mean of the relative matching error of each Task
    taken two at a time.
    See the docstring of _matching_error for details about the computation of the
    matching error between two tasks.
    """
    # We need exact values for begin, end and length of tasks, so force load them
    for task in tasks:
        task.force_load()

    matchingslice_begin, matchingslice_end = _find_matchingslice(tasks)
    nb_tasks = len(tasks)

    cumulated_badness = 0

    for task1, task2 in combinations(tasks, 2):
        # We compute the matching error over the common date slice of all tasks,
        # instead of just the slice that is common to the two current tasks.
        cumulated_badness += _matching_error(task1, task2, matchingslice_begin,
                                             matchingslice_end)

    return cumulated_badness / nb_tasks

def _matching_error(task1, task2, begin=None, end=None):
    """Compute the relative matching error between two Tasks.

    Ideally, two matching Tasks have measure traces, such that the trace of one
    is the image by an affine transformation of the other. Thus, the derivative
    of the first trace, is the derivative of the second trace scaled by a
    coefficient named a.
    For each record date (between begin and end if not None, otherwise over the
    matching timespan), we compute the derivative of each trace: their ratio
    gives the scaling coefficient a. We can then compute the standard deviation
    of this series of coefficients. It is rendered relative, by dividing by the
    difference between the biggest value and the lowest value of a.
    """
    # Honor given date slice bounds, and complete them if needed
    if begin is None or end is None:
        matchingslice_begin, matchingslice_end = _find_matchingslice([task1, task2])
        if begin is None:
            begin = matchingslice_begin
        if end is None:
            end = matchingslice_end

    # Compute the scaling coefficient on each date of the timespan
    affine_coeff = {}
    for date in range(begin, end):
        try:
            task1_prime = task1.measures[date+1] - task1.measures[date]
        except KeyError:
            raise TaskMeasuresInconsistencyError(task1)
        try:
            task2_prime = task2.measures[date+1] - task2.measures[date]
        except KeyError:
            raise TaskMeasuresInconsistencyError(task2)
        affine_coeff[date] = task2_prime / task1_prime if task1_prime != 0 else 1
    # Compute components of the variance
    mean = fsum(affine_coeff.values()) / (end - begin)
    sqr_mean = fsum([coeff**2 for coeff in affine_coeff.values()]) / (end - begin)
    # Compute the standard deviation (which is sqrt(variance)), and make it relative
    return sqrt(sqr_mean - mean**2) / (max(affine_coeff) - min(affine_coeff))

def get_bestmatchingtasks(tasks, nb_tasks, nb_parents, rel_matchingerror_thresh):
    """Find the best matching set of Tasks.

    Return a set of nb_tasks Tasks such that all are matching with a relative
    error less than threshold rel_matchingerror_thresh.

    Matching tasks, are nb_tasks tasks that share a common timespan of measure
    records, and such that their matching error is less than
    rel_matchingerror_thresh. The returned set, it the one among
    nb_parents computed sets with the lowest matching error.
    """
    cur_tasks = tasks.copy()
    parents = []

    # Select the parents for the tried sets of matching Tasks.
    for _ in range(nb_parents):
        # let it raise NoNestedTask if it has to
        parent = _find_biggest_nested(cur_tasks)
        cur_tasks -= {parent}
        parents.append(parent)
    logging.info('Selected parents')

    bestmatching_error = None
    progress = 0
    for parent in parents:
        progress += 1
        logging.info('Searching with parent %d (%f%%)',
                     progress, 100 * progress / nb_parents)
        try:
            # Search on cur_tasks to ignore parents, it would return duplicates
            matching_tasks = {parent} | get_matchingtasks(
                cur_tasks, nb_tasks - 1, rel_matchingerror_thresh, parent)
        except NoNestedTask:
            # Searching failed with the given parent, let's move forward
            # We also could find a new parent to replace it, just like we do
            # when handling TaskMeasuresInconsistencyError below.
            continue
        try:
            error = matching_error(matching_tasks)
        except TaskMeasuresInconsistencyError:
            try:
                new_parent = _find_biggest_nested(cur_tasks)
            except NoNestedTask:
                # That's too bad, we can't replace the inconsistent Task so we
                # just move forward.
                continue
            else:
                cur_tasks -= {new_parent}
                parents.append(new_parent)
                nb_parents += 1
        else:
            if bestmatching_error is None or error < bestmatching_error:
                best_matching = matching_tasks
                bestmatching_error = error

    return best_matching

def main():
    """Main method of the module."""
    try:
        matching_tasks = get_bestmatchingtasks(
            set(read_traces(sys.argv[1])),
            int(sys.argv[2]), int(sys.argv[3]), float(sys.argv[4]))
    except IndexError:
        print(globals()['__doc__'])
        exit(2)
    except NoNestedTask:
        logging.error('Couldn\'t find enough nested tasks :(')
        exit(1)

    print('\n'.join(str(task.job_id) + '_' + str(task.task_id)
                    for task in matching_tasks))

    print('Done!')

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
