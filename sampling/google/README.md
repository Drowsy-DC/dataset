Google dataset sampler
======================

Paraphrasing the README file in the Google dataset folder, "it requires a lot of work to extract interesting traces". This is reflected in the number of steps and technologies required to do this work:
 1. filter traces to identify traces of tasks with a long-enough lifespan;
 * convert and fix traces, from _approximately_ one measure every 5min, to a cumulated measure for each hour;
 * filter converted traces that include too much bad records (records with a resource measure of 0.0);
 * find matching tasks, i.e. tasks that exhibit similar inactivity patterns;
 * extract the traces of these tasks.

The two first steps are in the form of [Hadoop](http://hadoop.apache.org/) jobs, the following two are Python scripts, and the last one is a simple Bash script.

## Filter traces

First, it is necessary to filter traces to identify the ones that belongs to tasks with a long-enough lifespan: indeed, we are looking for _long-lived_ tasks.

The result of this first step, is a list of task IDs (actually, job IDs and task IDs relative to the job) alongside begin and end dates of each task.

I will let you setup a Hadoop cluster (at least version 2; the job ran with Hadoop 2.2). The job source code is in the "com" folder, so compile it and create a Jar from it:
```
# you may need to provide the path to Hadoop libraries using the `-classpath` option
javac `find com -name "*.java"`
jar -cvf GoogleSampling.jar `find com -name "*.class"`
```

The job class is `TaskLengthFilter`. It requires first the path to the Google **task events** traces folder (no need to unzip them), and second an output path:
```
hadoop jar GoogleSampling.jar com.drowsydc.sampling.google.TaskLengthFilter <task events> <output>
```

After the work, the output directory shall include several Gzip-ed files, which contain a list of task ID and begin and end dates, such as:
```
6333623939_53	861395592119	2236854176642
```
(you can use zcat or zgrep to extract data from the files)

`6333623939_53` is the task ID, made of the job ID (`6333623939`) and the task ID relative to the job (`53`). `861395592119` is the begin date of the task (in microseconds, as expressed in the Google traces), and `2236854176642` is the end date. Each field is separated by a tabulation `\t`.

While you are at it, **unzip the files:** this is required for the next step. You can run the script "unzip_filtered_traces.sh" to do so, it just needs the path in HDFS to the filtered tasks folder as its parameter.

## Convert and fix traces

The next step, a Hadoop job as well, is about extracting the traces of the eligible tasks and converting them to a suitable format. The "suitable format" is simply that we accumulate measure records from the dataset to produce a measure for each hour; whereas the dataset _theoretically_ includes a measure every five minutes. Some measure records may be missing, and we try our best to fill them in using linear interpolation.

The result of this second step, is trace files containing for each of them, the converted resource usage trace of a task.

The job class is `TaskHourlyConverter`, under the same package as the previous job. It requires first the path to the Google **task usage** traces folder, second the path to the folder containing the **unzipped** files output by the first job, and third an output path:
```
hadoop jar GoogleSampling.jar com.drowsydc.sampling.google.TaskHourlyConverter <task usages> <filtered tasks> <output>
```

After the work, the output directory shall include several folders, named after the _job ID_. Each of them include files, named after the _task ID_ (a non-determinist suffix is appended by Hadoop under the form "-r-XXXXX", where Xs are digits). Each file is the converted resource usage trace of the task, where each line is a record under the form:
```
6502    1.2 0.45
```
(again, use zcat or zgrep)

`6502` is the hour of the record, `1.2` is the CPU usage over the hour, and `0.45` is the memory usage over the hour. Resurce usage (both CPU and memory) are expressed as _resource.hour_ (as in "CPU consumption times one hour"). Google dataset does not include absolute resource consumption values, only relative values. As the Hadoop job scans dataset records, it accumulates 5min records (relative, average resource consumption over 5 minutes) and express them with respect to the timespan is covers since the last record (5 minutes is a majority of cases). Again, each field is separated by a tabulation.

You need not unzip the files for the next step.

## Filter converted tasks with too much bad records

Again, Google dataset may include failures in resource consumption measure recording, resulting in either missing records (handled in the previous step) or in null records, where the resource usage is exactly 0.0. While the Google dataset documentation states that there are legitimate cases where such a situation may occur (e.g. binaries are being copied into the task container), we consider all such cases as erroneous, as they cannot be exploited.

The result of this third step, is a converted traces folder expunged from "bad" traces, and a secondary folder containing the said bad traces.

This work is achieved by a Python 3 script named "missingrecords_taskfilter.py". It requires first the path to the converted traces (the output of the previous step, **not unzipped**), second an output directory to move bad traces into, and third a relative badness threshold:
```
./missingrecords_taskfilter.py <converted traces> <bad traces output folder> <badness threshold>
```

The badness threshold, is the relative value above which a trace is deemed bad. Badness is the ratio between the number of bad records, and the total number of records: e.g. a threshold of `0.1` will remove traces with at least 10% of bad records.

## Find matching tasks

The fourth step will scan the converted, good-enough task traces to find matching tasks. Matching tasks are tasks with:
 * measure records during the same period of time;
 * similar behavior of inactivity.

The result of this fourth step, is a list of task IDs output to the standard output; these tasks show matching behavior.

This work is achieved by another Python 3 script named "matching_tasks.py". It requires first the path to the converted traces (again, it is the **non unzipped** output of the third step, possibly expunged of bad traces by the third step), second a number of matching tasks queried, third a number of tried tasks, an fourth a relative matching error threshold:
```
./matching_tasks.py <converted traces> <matching tasks number> <tried tasks number> <relative matching error threshold> > output_file
```

 * the matching task number, is the number of matching tasks the script shall find;
 * the tried tasks number is the number of tasks that are elected first; then, the script tries to find enough matching tasks for each of them, and will output the best matching set of tasks. In other words, this parameter tells the script how hard it should look for matching tasks; a good number is 10% of the total number of converted traces, depending on the time you wish to let it run for;
 * the relative matching error threshold, tells the script how close the tasks should be from one another; 0.1 (i.e. 10%) means that the script will deem a task matching, if it has at most 10% of value difference with the previously elected one. Note that whatever this criterium is, the script will output only the best matching set of tasks among all the tried tasks.

It is better to store the result in a file (it is output to standard output otherwise), as it will be used by the next step.

## Extract matching tasks

Finally, the fifth and last step is very simple. The Bash script "get_matching_tasks.sh" will read the list of matching tasks output by the previous step, and copy the corresponding traces into an output folder.

The script requires first the path to the converted traces (it will look for Gzipp-ed files), second the path to the _existing_ output directory, and third, an optional name of the file that contains the matching tasks IDs as output by the previous step:
```
./get_matching_tasks.sh <converted traces> <output> <matching tasks>
```

If the last parameter is not present, the script reads the list from standard input so you can do the following:
```
./matching_tasks.py <converted traces> <matching tasks number> <tried tasks number> <relative matching error threshold> | ./get_matching_tasks.sh <converted traces> <output>
```

The output directory contains only the files of the matching traces.
