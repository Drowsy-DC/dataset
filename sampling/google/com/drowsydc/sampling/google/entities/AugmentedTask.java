package com.drowsydc.sampling.google.entities;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.WritableComparable;

/** Representation of a Task augmented with other elements.

It takes care of managing the Task along with supplementary elements. The comparison
is done first according to the Task, and then is delegated to the comparison of
the supplementary elements. The hash code is exactly the hash of the Task.

It is a WritableComparable, usable with Hadoop as a key. */
public abstract class AugmentedTask<T>
        implements WritableComparable<AugmentedTask<T>> {
    protected Task task;

    protected AugmentedTask () {
        this.task = new Task();
    }

    @Override
    public void readFields (DataInput input) throws IOException {
        this.task.readFields(input);
        this.readFieldsAugmented(input);
    }

    public abstract void readFieldsAugmented (DataInput input) throws IOException;

    @Override
    public void write (DataOutput output) throws IOException {
        this.task.write(output);
        this.writeAugmented(output);
    }

    public abstract void writeAugmented (DataOutput output) throws IOException;

    public void setJobID (long id) {
        this.task.setJobID(id);
    }

    public void setTaskID (int id) {
        this.task.setTaskID(id);
    }

    public long getJobID () {
        return this.task.getJobID();
    }

    public int getTaskID () {
        return this.task.getTaskID();
    }

    public Task getTask () {
        return this.task;
    }

    @SuppressWarnings("unchecked")
    @Override
    public int compareTo (AugmentedTask<T> other) {
        int comp = this.task.compareTo(other.task);
        if (comp == 0) {
            comp = this.compareToAugmented((T) other);
        }

        return comp;
    }

    public int compareByTaskTo (AugmentedTask<T> other) {
        return this.task.compareTo(other.task);
    }

    public abstract int compareToAugmented (T other);

    /** Return a hashed representation of this.

    The hash is only based on the underlying Task: it ignores augmented fields. */
    @Override
    public int hashCode () {
        return this.task.hashCode();
    }

    /** String representation of the augmented task.

    It defaults to the String representation of the underlying task. */
    @Override
    public String toString () {
        return this.task.toString();
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals (Object other) {
        return compareTo((AugmentedTask<T>) other) == 0;
    }
}
