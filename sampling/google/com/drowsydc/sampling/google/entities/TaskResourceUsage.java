package com.drowsydc.sampling.google.entities;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

public class TaskResourceUsage implements Writable {
    private LongWritable timestamp = new LongWritable();
    private FloatWritable avrgCPU = new FloatWritable();
    private FloatWritable avrgMem = new FloatWritable();

    public void copyFrom (TaskResourceUsage from) {
        this.timestamp.set(from.timestamp.get());
        this.avrgCPU.set(from.avrgCPU.get());
        this.avrgMem.set(from.avrgMem.get());
    }

    @Override
    public void readFields (DataInput input) throws IOException {
        this.timestamp.readFields(input);
        this.avrgCPU.readFields(input);
        this.avrgMem.readFields(input);
    }

    @Override
    public void write (DataOutput output) throws IOException {
        this.timestamp.write(output);
        this.avrgCPU.write(output);
        this.avrgMem.write(output);
    }

    public void setTimestamp (long ts) {
        this.timestamp.set(ts);
    }

    public void setAvrgCPU (float avrg) {
        this.avrgCPU.set(avrg);
    }

    public void setAvrgMem (float avrg) {
        this.avrgMem.set(avrg);
    }

    public long getTimestamp () {
        return this.timestamp.get();
    }

    public float getAvrgCPU () {
        return this.avrgCPU.get();
    }

    public float getAvrgMem () {
        return this.avrgMem.get();
    }

    @Override
    public String toString () {
        return this.timestamp.toString() + "\t" + this.avrgCPU.toString() +
            "\t" + this.avrgMem.toString();
    }
}
