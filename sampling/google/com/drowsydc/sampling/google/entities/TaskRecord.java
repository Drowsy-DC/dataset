package com.drowsydc.sampling.google.entities;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collection;
import org.apache.hadoop.io.LongWritable;

/** A generic AugmentedTask class that augment a task with a timestamp.

Useful to do a secondary sort on the task ID and then on the timestamp of the
record, when coupled with TaskRecordGroupingComparator. */
public class TaskRecord extends AugmentedTask<TaskRecord> {
    private LongWritable timestamp = new LongWritable();

    @Override
    public void readFieldsAugmented (DataInput input) throws IOException {
        this.timestamp.readFields(input);
    }

    @Override
    public void writeAugmented (DataOutput output) throws IOException {
        this.timestamp.write(output);
    }

    public void setTimestamp (long timestamp) {
        this.timestamp.set(timestamp);
    }

    public long getTimestamp () {
        return this.timestamp.get();
    }

    @Override
    public int compareToAugmented (TaskRecord other) {
        long thisTimestamp = this.timestamp.get();
        long otherTimestamp = other.timestamp.get();
        return (thisTimestamp < otherTimestamp ? -1 :
            (thisTimestamp == otherTimestamp ? 0 : 1));
    }

    public boolean isInTaskLives (Collection<TaskLife> lives) {
        // no lives for this task: is not long enough
        if (lives == null)
            return false;

        long thisTimestamp = this.timestamp.get();
        for (TaskLife life: lives) {
            if (thisTimestamp >= life.getCreationTimestamp() &&
                    thisTimestamp <= life.getDestructionTimestamp())
                return true;
        }
        return false;
    }
}
