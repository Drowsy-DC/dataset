package com.drowsydc.sampling.google.entities;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;

public class Task implements WritableComparable<Task> {
    private LongWritable jobID = new LongWritable();
    private IntWritable taskID = new IntWritable();

    public Task () {}

    public static Task read (DataInput input) throws IOException {
        Task ret = new Task();
        ret.readFields(input);
        return ret;
    }

    @Override
    public void readFields (DataInput input) throws IOException {
        this.jobID.readFields(input);
        this.taskID.readFields(input);
    }

    @Override
    public void write (DataOutput output) throws IOException {
        this.jobID.write(output);
        this.taskID.write(output);
    }

    public void setJobID (long id) {
        this.jobID.set(id);
    }

    public void setTaskID (int id) {
        this.taskID.set(id);
    }

    public long getJobID () {
        return this.jobID.get();
    }

    public int getTaskID () {
        return this.taskID.get();
    }

    @Override
    public int compareTo (Task other) {
        long thisJobID = this.jobID.get();
        long otherJobID = other.jobID.get();
        int comp = (thisJobID < otherJobID ? -1 :
            (thisJobID == otherJobID ? 0 : 1));
        if (comp == 0) {        // same job ID
            int thisTaskID = this.taskID.get();
            int otherTaskID = other.taskID.get();
            comp = (thisTaskID < otherTaskID ? -1 :
                (thisTaskID == otherTaskID ? 0 : 1));
        }

        return comp;
    }

    @Override
    public int hashCode () {
        return this.jobID.hashCode() + this.taskID.hashCode();
    }

    @Override
    public String toString () {
        return Long.toString(this.jobID.get()) + "_" +
            Integer.toString(this.taskID.get());
    }

    @Override
    public boolean equals (Object other) {
        return this.compareTo((Task) other) == 0;
    }
}
