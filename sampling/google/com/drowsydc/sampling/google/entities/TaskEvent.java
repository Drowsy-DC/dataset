package com.drowsydc.sampling.google.entities;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

public class TaskEvent implements Writable {
    private LongWritable timestamp = new LongWritable();
    private IntWritable type = new IntWritable();

    @Override
    public void readFields (DataInput input) throws IOException {
        this.timestamp.readFields(input);
        this.type.readFields(input);
    }

    @Override
    public void write (DataOutput output) throws IOException {
        this.timestamp.write(output);
        this.type.write(output);
    }

    public void setTimestamp (long ts) {
        this.timestamp.set(ts);
    }

    public void setType (int type) {
        this.type.set(type);
    }

    public long getTimestamp () {
        return this.timestamp.get();
    }

    public int getType () {
        return this.type.get();
    }

    @Override
    public String toString () {
        return this.timestamp.toString() + "\t" + this.type.toString();
    }
}
