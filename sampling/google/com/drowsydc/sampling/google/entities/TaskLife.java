package com.drowsydc.sampling.google.entities;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

public class TaskLife implements Writable {
    private LongWritable creationTimestamp = new LongWritable();
    private LongWritable destructionTimestamp = new LongWritable();

    @Override
    public void readFields (DataInput input) throws IOException {
        this.creationTimestamp.readFields(input);
        this.destructionTimestamp.readFields(input);
    }

    @Override
    public void write (DataOutput output) throws IOException {
        this.creationTimestamp.write(output);
        this.destructionTimestamp.write(output);
    }

    public void setCreationTimestamp (long ts) {
        this.creationTimestamp.set(ts);
    }

    public void setDestructionTimestamp (long ts) {
        this.destructionTimestamp.set(ts);
    }

    public long getCreationTimestamp () {
        return this.creationTimestamp.get();
    }

    public long getDestructionTimestamp () {
        return this.destructionTimestamp.get();
    }

    @Override
    public String toString () {
        return this.creationTimestamp.toString() + "\t" +
            this.destructionTimestamp.toString();
    }
}
