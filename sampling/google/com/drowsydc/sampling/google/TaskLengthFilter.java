package com.drowsydc.sampling.google;

import com.drowsydc.sampling.google.entities.Task;
import com.drowsydc.sampling.google.entities.TaskRecord;
import com.drowsydc.sampling.google.entities.TaskEvent;
import com.drowsydc.sampling.google.entities.TaskLife;
import com.drowsydc.sampling.google.lib.TaskRecordGroupingComparator;
import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TaskLengthFilter {
    public static class TaskEventFilterMapper
            extends Mapper<Object, Text, TaskRecord, TaskEvent> {
        private TaskRecord record = new TaskRecord();
        private TaskEvent event = new TaskEvent();

        @Override
        public void map (Object key, Text value, Context context)
                throws IOException, InterruptedException {
            String[] line = value.toString().split(",");
            long timestamp;

            try {
                timestamp = Long.parseLong(line[0]);
            } catch (NumberFormatException err) {
                /* Timestamps of the "infinite" in the traces, have a value of
                2^64-1 (and **not** 2^63-1 unlike stated in the documentation).
                This value does not fit into a long. I don't care about these
                records because they cannot be creation events as they happen
                after the end of the measure records, so I ignore them. */
                return;
            }
            if (timestamp == 0) {
                /* Ignore events dating before the measurements, which would
                yield unreal durations. */
                return;
            }

            int eventType = Integer.parseInt(line[5]);
            /* Filter out unwanted events */
            if (!(eventType >= 1 && eventType <= 6))
                return;

            record.setJobID(Long.parseLong(line[2]));
            record.setTaskID(Integer.parseInt(line[3]));
            record.setTimestamp(timestamp);

            event.setTimestamp(timestamp);
            event.setType(eventType);

            context.write(record, event);
        }
    }

    public static class TaskLifeReducer
            extends Reducer<TaskRecord, TaskEvent, Task, TaskLife> {
        /** Tasks whose length is less than this value, are deemed "not long
        enough" and are not output.
        Current value is: 6 months of 30 days, converted to traces timestamps
        that are in microseconds, with the ratio 1h -> 5min (so that's 15 days of
        Google traces).
        You understood that correctly: Google traces are expanded from less than
        a month, to less than a year, with 5min of Google traces equivalent
        to 1h of simulation traces (so about 15 days of Google traces are worth
        6 months of simulation traces).*/
        // OMFG THE `L` IS IMPORTANT, OTHERWISE IT COMPUTES AS INT, WHICH OVERFLOWS
        private static final long TASKLENGTH_THRESHOLD = 6 * 30 * 24 * 5 *
            60000000L;

        private static enum TaskEventTypes {
            UNDEFINED, CREATION, DESTRUCTION;

            public static TaskEventTypes parseType (int type) {
                switch (type) {
                    case 1:
                        return CREATION;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return DESTRUCTION;
                    default:
                        /* shouldn't happen in this context: the mapper only
                        outputs creation and destruction events */
                        return UNDEFINED;
                }
            }
        }

        private TaskLife life = new TaskLife();

        @Override
        public void reduce (TaskRecord record, Iterable<TaskEvent> events,
                Context context) throws IOException, InterruptedException {
            Iterator<TaskEvent> eventsIter = events.iterator();
            TaskEvent temp;
            long firstTS, secondTS;

            /* Although job IDs are not (supposed) to be reused (I know there is
            one known case of this throughout the traces, but... Whatever), task
            IDs **are reused** in their job (because a task ID is just an index
            local to the job). So there can be more than one couple of creation-
            destruction timestamps.*/
            while (eventsIter.hasNext()) {
                /* Hadoop really does some nasty stuff with the references in
                its iterable (reuses the same), so it is always preferable to
                get the native data as soon as possible. */
                temp = eventsIter.next();

                /* We are supposed to iterate couples of "creation-destruction"
                events, but in real life a task may be destroyed before creation.
                We just ignore such cases and go on. */
                if (TaskEventTypes.parseType(temp.getType()) !=
                        TaskEventTypes.CREATION)
                    continue;

                firstTS = temp.getTimestamp();

                /* We iterate by couples of timestamps, where the first should be
                the creation of the task, and the second its destruction. We
                shouldn't reach the end on the list on an odd number because we
                check that the events types follow the sequence "creation-
                destruction" and we skip records that do not respect this; so
                this condition should never be false (unless the last record is
                one that does not respect the sequence), but let's be safe. */
                if (eventsIter.hasNext()) {
                    temp = eventsIter.next();

                    /* According to the documentation, there is no way the event
                    after a creation could not be a destruction. But let's be
                    safe here too. */
                    if (TaskEventTypes.parseType(temp.getType()) !=
                            TaskEventTypes.DESTRUCTION)
                        continue;

                    secondTS = temp.getTimestamp();

                    if (secondTS - firstTS >= TASKLENGTH_THRESHOLD) {
                        life.setCreationTimestamp(firstTS);
                        life.setDestructionTimestamp(secondTS);

                        context.write(record.getTask(), life);
                    }
                }
            }
        }
    }

    public static void main (String[] args) throws Exception {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "Google traces tasks length filter");
        job.setJarByClass(TaskLengthFilter.class);
        job.setMapperClass(TaskEventFilterMapper.class);
        job.setGroupingComparatorClass(TaskRecordGroupingComparator.class);
        job.setReducerClass(TaskLifeReducer.class);
        job.setMapOutputKeyClass(TaskRecord.class);
        job.setMapOutputValueClass(TaskEvent.class);
        job.setOutputKeyClass(Task.class);
        job.setOutputValueClass(TaskLife.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
