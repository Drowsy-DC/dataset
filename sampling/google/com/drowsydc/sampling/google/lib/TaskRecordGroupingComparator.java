package com.drowsydc.sampling.google.lib;

import com.drowsydc.sampling.google.entities.TaskRecord;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/** Grouping comparator based solely on task ID.

Useful to do a secondary sort on the task ID and then on the timestamp of the
record, because this grouping ignore the timestamp of the TaskRecord. */
public class TaskRecordGroupingComparator extends WritableComparator {
    public TaskRecordGroupingComparator () {
        super(TaskRecord.class, true);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public int compare (WritableComparable record1, WritableComparable record2) {
        return ((TaskRecord) record1).compareByTaskTo((TaskRecord) record2);
    }
}
