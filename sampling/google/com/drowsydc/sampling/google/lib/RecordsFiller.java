package com.drowsydc.sampling.google.lib;

import com.drowsydc.sampling.google.entities.TaskResourceUsage;
import java.util.Iterator;

public class RecordsFiller implements Iterable<TaskResourceUsage> {
    private TaskResourceUsage lastTRU;
    private TaskResourceUsage currentTRU;
    private float delta_cpu = 0.0f;
    private float delta_mem = 0.0f;

    public RecordsFiller (TaskResourceUsage lastTRU,
            TaskResourceUsage currentTRU) throws NoMissingRecordsException {
        if (!(currentTRU.getTimestamp() > lastTRU.getTimestamp() + 1))
            throw new NoMissingRecordsException();

        this.lastTRU = lastTRU;
        this.currentTRU = currentTRU;
        this.delta_cpu = (currentTRU.getAvrgCPU() - lastTRU.getAvrgCPU()) /
            (currentTRU.getTimestamp() - lastTRU.getTimestamp());
        this.delta_mem = (currentTRU.getAvrgMem() - lastTRU.getAvrgMem()) /
            (currentTRU.getTimestamp() - lastTRU.getTimestamp());
    }

    @Override
    public Iterator<TaskResourceUsage> iterator () {
        return new Iterator<TaskResourceUsage> () {
            private TaskResourceUsage interpTRU = new TaskResourceUsage();
            private long hour = 1;

            @Override
            public boolean hasNext () {
                return hour < currentTRU.getTimestamp() - lastTRU.getTimestamp();
            }

            @Override
            public TaskResourceUsage next () {
                interpTRU.setTimestamp(lastTRU.getTimestamp() + hour);
                interpTRU.setAvrgCPU(lastTRU.getAvrgCPU() + hour * delta_cpu);
                interpTRU.setAvrgMem(lastTRU.getAvrgMem() + hour * delta_cpu);

                hour++;

                return interpTRU;
            }

            @Override
            public void remove () {
                throw new UnsupportedOperationException();
            }
        };
    }
}
