package com.drowsydc.sampling.google;

import com.drowsydc.sampling.google.entities.Task;
import com.drowsydc.sampling.google.entities.TaskRecord;
import com.drowsydc.sampling.google.entities.TaskLife;
import com.drowsydc.sampling.google.entities.TaskResourceUsage;
import com.drowsydc.sampling.google.lib.RecordsFiller;
import com.drowsydc.sampling.google.lib.NoMissingRecordsException;
import com.drowsydc.sampling.google.lib.TaskRecordGroupingComparator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TaskHourlyConverter {
    public static class TaskUsageMapper
            extends Mapper<Object, Text, TaskRecord, TaskResourceUsage> {
        private TaskRecord measure = new TaskRecord();
        private TaskResourceUsage taskResUsage = new TaskResourceUsage();

        private HashMap<Task, ArrayList<TaskLife>> longTasks =
            new HashMap<Task, ArrayList<TaskLife>>(5000);

        @Override
        public void setup (Context context)
                throws IOException, InterruptedException {
            super.setup(context);
            URI[] taskListParts = context.getCacheArchives();
            BufferedReader br;
            String lineRaw;
            String[] line, taskID;
            Task task;
            Task prevTask = null;
            TaskLife life;
            ArrayList<TaskLife> lives = new ArrayList<TaskLife>();
            FileSystem fs = FileSystem.get(context.getConfiguration());

            for (URI taskListPart: taskListParts) {
                System.out.println("Reading input task list part " +
                    taskListPart.toString());
                br = new BufferedReader(new InputStreamReader(
                    fs.open(new Path(taskListPart))));
                /* We accumulate lives of a given task (don't forget that task
                IDs can get reassigned in a given job, so a task might have
                multiple lives). When we encounter a different task, we store the
                accumulated lives. */
                while ((lineRaw = br.readLine()) != null) {
                    line = lineRaw.split("\\t");

                    taskID = line[0].split("_");
                    task = new Task();
                    task.setJobID(Long.parseLong(taskID[0]));
                    task.setTaskID(Integer.parseInt(taskID[1]));
                    if (prevTask != null && prevTask.compareTo(task) != 0) {
                        longTasks.put(prevTask, lives);
                        lives = new ArrayList<TaskLife>();
                    }

                    life = new TaskLife();
                    life.setCreationTimestamp(Long.parseLong(line[1]));
                    life.setDestructionTimestamp(Long.parseLong(line[2]));

                    lives.add(life);
                    prevTask = task;
                }
                longTasks.put(prevTask, lives);
                br.close();
            }
            System.out.println("Read " + longTasks.size() + " long tasks IDs");
            if (longTasks.size() == 0) {
                System.out.println("So we're done, ciao!");
                System.exit(1);
            }
        }

        @Override
        public void map (Object key, Text value, Context context)
                throws IOException, InterruptedException {
            String[] line = value.toString().split(",");
            long timestamp = Long.parseLong(line[0]);

            measure.setJobID(Long.parseLong(line[2]));
            measure.setTaskID(Integer.parseInt(line[3]));
            measure.setTimestamp(timestamp);
            /* Check that the record is interesting (about an activity that is
            long enough).
            Note that lives' timestamps are expressed in the Google traces
            format, i.e. they have not been converted. */
            if (!measure.isInTaskLives(longTasks.get(measure.getTask())))
                return;
            taskResUsage.setTimestamp(timestamp);
            taskResUsage.setAvrgCPU(Float.parseFloat(line[5]));
            taskResUsage.setAvrgMem(Float.parseFloat(line[6]));

            context.write(measure, taskResUsage);
        }

        @Override
        public void cleanup (Context context) {
            longTasks = null;
        }
    }

    public static class TaskReducer
            extends Reducer<TaskRecord, TaskResourceUsage, NullWritable,
                TaskResourceUsage> {
        private NullWritable outputNullKey = NullWritable.get();
        private TaskResourceUsage lastTRU = new TaskResourceUsage();
        private TaskResourceUsage lastWrittenTRU = new TaskResourceUsage();
        private TaskResourceUsage accumulatedTRU = new TaskResourceUsage();

        private MultipleOutputs<NullWritable, TaskResourceUsage> out;

        @Override
        public void setup (Context context)
                throws IOException, InterruptedException {
            super.setup(context);
            out = new MultipleOutputs<NullWritable, TaskResourceUsage>(context);
        }

        /**

        Output values are in resource.hour: it's the cumulated result of the
        value of each record multiplied by the time delta between the successive
        records. When accumulating, the value is resource.minute; upon writing,
        the value is divided by 60 to make it resource.hour.
        */
        @Override
        public void reduce (TaskRecord taskMeas, Iterable<TaskResourceUsage> trus,
                Context context) throws IOException, InterruptedException {
            TaskResourceUsage tru;
            String filename = generateFileName(taskMeas.getTask());
            Iterator<TaskResourceUsage> trusIter = trus.iterator();
            long cumulatedTime;

            /* TRUs have timestamp in microseconds */
            lastTRU.copyFrom(trusIter.next());
            /* The last written and the accumulated TRU have timestamp in hour. */
            /* Set the hour of the "last written TRU" to the hour of the first
            one, so it cannot trigger interpolation until a true last written
            TRU is set. */
            lastWrittenTRU.setTimestamp(lastTRU.getTimestamp() / (3600 * 1000000L));
            accumulatedTRU.setTimestamp(lastWrittenTRU.getTimestamp());
            accumulatedTRU.setAvrgCPU(lastTRU.getAvrgCPU() *
                (lastTRU.getTimestamp() / (60 * 1000000L) -
                accumulatedTRU.getTimestamp() * 60L));
            accumulatedTRU.setAvrgMem(lastTRU.getAvrgMem() *
                (lastTRU.getTimestamp() / (60 * 1000000L) -
                accumulatedTRU.getTimestamp() * 60L));

            while (trusIter.hasNext()) {
                tru = trusIter.next();

                /* If we reached the next hour */
                if (tru.getTimestamp() / (3600 * 1000000L) !=
                        accumulatedTRU.getTimestamp()) {
                    /* Last accumulation of resource usage, from the last record
                    to the end of the hour */
                    /* If tru is not during the next hour, but later (meaning
                    there are missing records): we do not change the
                    accumulatedTRU, and we average it over the cumulated time
                    (which is less than a full hour in this case, since records
                    are not contiguous). */
                    if (tru.getTimestamp() / (3600 * 1000000L) >
                            accumulatedTRU.getTimestamp() + 1) {
                        cumulatedTime = lastTRU.getTimestamp() / (60 * 1000000L)
                            - accumulatedTRU.getTimestamp() * 60L;
                        accumulatedTRU.setAvrgCPU(accumulatedTRU.getAvrgCPU() /
                            cumulatedTime);
                        accumulatedTRU.setAvrgMem(accumulatedTRU.getAvrgMem() /
                            cumulatedTime);
                    } else {
                        accumulatedTRU.setAvrgCPU(accumulatedTRU.getAvrgCPU() +
                            tru.getAvrgCPU() * ((accumulatedTRU.getTimestamp() + 1) *
                            60L - lastTRU.getTimestamp() / (60 * 1000000L)));
                        accumulatedTRU.setAvrgMem(accumulatedTRU.getAvrgMem() +
                            tru.getAvrgMem() * ((accumulatedTRU.getTimestamp() + 1) *
                            60L - lastTRU.getTimestamp() / (60 * 1000000L)));

                        accumulatedTRU.setAvrgCPU(accumulatedTRU.getAvrgCPU() / 60);
                        accumulatedTRU.setAvrgMem(accumulatedTRU.getAvrgMem() / 60);
                    }

                    /* Some measures may be missing, so we fill missing records
                    using linear interpolation, if needed.
                    The RecordsFiller raises an exception if there is no need to
                    interpolate because no record is missing. */
                    try {
                        for (TaskResourceUsage fillerTRU: new RecordsFiller(
                                lastWrittenTRU, accumulatedTRU))
                            out.write(outputNullKey, fillerTRU, filename);
                    } catch (NoMissingRecordsException err) {} // nothing to do

                    out.write(outputNullKey, accumulatedTRU, filename);

                    lastWrittenTRU.copyFrom(accumulatedTRU);
                    /* Set last timestamp to the beginning of the new hour.
                    Because of the rounding, the computation does not cancel
                    itself: there's no error. */
                    lastTRU.setTimestamp((tru.getTimestamp() / (3600 * 1000000L)) *
                        (3600 * 1000000L));

                    accumulatedTRU.setTimestamp(lastTRU.getTimestamp());
                    accumulatedTRU.setAvrgCPU(0);
                    accumulatedTRU.setAvrgMem(0);
                }

                accumulatedTRU.setAvrgCPU(accumulatedTRU.getAvrgCPU() +
                    tru.getAvrgCPU() * (tru.getTimestamp() / (60 * 1000000L) -
                    lastTRU.getTimestamp() / (60 * 1000000L)));
                accumulatedTRU.setAvrgMem(accumulatedTRU.getAvrgMem() +
                    tru.getAvrgMem() * (tru.getTimestamp() / (60 * 1000000L) -
                    lastTRU.getTimestamp() / (60 * 1000000L)));

                lastTRU.copyFrom(tru);
            }

            /* Handle last accumulated TRU */
            cumulatedTime = lastTRU.getTimestamp() / (60 * 1000000L) -
                accumulatedTRU.getTimestamp() * 60L;
            accumulatedTRU.setAvrgCPU(accumulatedTRU.getAvrgCPU() / cumulatedTime);
            accumulatedTRU.setAvrgMem(accumulatedTRU.getAvrgMem() / cumulatedTime);
            try {
                for (TaskResourceUsage fillerTRU: new RecordsFiller(
                        lastWrittenTRU, accumulatedTRU))
                    out.write(outputNullKey, fillerTRU, filename);
            } catch (NoMissingRecordsException err) {} // nothing to do

            out.write(outputNullKey, accumulatedTRU, filename);
        }

        private String generateFileName (Task task) {
            return Long.toString(task.getJobID()) + "/" +
                Integer.toString(task.getTaskID());
        }

        @Override
        public void cleanup (Context context)
                throws IOException, InterruptedException {
            out.close();
        }
    }

    public static void main (String[] args) throws Exception {
        Configuration conf = new Configuration();
        GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        String[] remainingArgs = optionParser.getRemainingArgs();
        if (remainingArgs.length != 3) {
            System.err.println("Usage: hadoop_job <in> <tasks> <out>");
            System.err.println("`tasks` is result directory of the TaskFilter job");
            System.exit(2);
        }

        Job job = Job.getInstance(conf, "Google traces tasks usage measures " +
            "converter");
        job.setJarByClass(TaskHourlyConverter.class);
        job.setMapperClass(TaskUsageMapper.class);
        job.setGroupingComparatorClass(TaskRecordGroupingComparator.class);
        job.setReducerClass(TaskReducer.class);
        job.setMapOutputKeyClass(TaskRecord.class);
        job.setMapOutputValueClass(TaskResourceUsage.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(TaskResourceUsage.class);
        LazyOutputFormat.setOutputFormatClass(job, TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(remainingArgs[0]));
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] fileStatus = fs.listStatus(new Path(remainingArgs[1]));
        try {
            for (FileStatus status: fileStatus) {
                URI taskListPartURI = status.getPath().toUri();
                job.addCacheArchive(taskListPartURI);
                System.out.println("Added " + taskListPartURI.toString() +
                    " to input filtered tasks.");
            }
        } catch (NullPointerException err) {
            System.err.println("`tasks` is not a directory");
            System.exit(2);
        }
        FileOutputFormat.setOutputPath(job, new Path(remainingArgs[2]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
