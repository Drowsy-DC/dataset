#!/bin/bash

if [ $# -lt 2 ]; then
	echo "Usage: $0 CONVERTED_TASKS OUTPUT_DIR [MATCHING_TASKS]"
	echo "CONVERTED_TASKS is the folder containing converted tasks, as output by \
	 	the Hadoop job TaskHourlyConverter"
	echo "OUTPUT_DIR is the output folder (must exists) matching tasks \
		traces are copied into"
	echo "MATCHING_TASKS is the input file containing the matching tasks IDs, as \
		output by matching_tasks.py; if not present, read the list from stdin"
	exit 2
fi

if [ ! -d $1 ]; then
	echo "First argument \"$1\" is not a directory, or does not exist."
	exit 2
fi
if [ ! -d $2 ]; then
	echo "Second argument \"$2\" is not a directory, or does not exist."
	exit 2
fi
[ $# -ge 3 -a -f "$3" ] && input="$3" || input="-"

ids=$(cat $input)

for id in $ids; do
	path=$(echo "$id" | sed 's!_!/!')
	cp "$1/$path"-r-*.gz "$2/"
done
