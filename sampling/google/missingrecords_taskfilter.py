#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Filter task traces with too many missing measure records.

This program moves trace files outside of the source folder, if the trace shows
too many erroneous records, i.e. records that are equal to 0.0.

Usage:
    ./missingrecords_taskfilter.py FILTERED_TASKS BADTRACES_DIR BADNESS_THRESH

 * FILTERED_TASKS is the directory containing the converted traces measures as
    output by the Hadoop job MeasureRecordsConverter
 * BADTRACES_DIR is the directory into which move bad traces
 * BADNESS_THRESH is the threshold of badness above which a trace is deemed bad.
    Badness is the ratio between the number of bad records, and the total number
    of records (0.1, i.e. 10% of bad records already gives good results)

According to the documentation, there **are** genuine situations where task
resource usage can be 0.0, for instance when "binaries are being copied to the
machine". In our case, this is all the same as illegitimately having a record
value of 0, for instance because the monitoring software failed.
"""


import glob
import shutil
import sys
import os
import gzip
import logging
from functools import partial
from multiprocessing import Pool


def check_tracequality_and_move(filename, rel_threshold, badtraces_dir):
    """Check the quality of the trace in a file, and move it if it is bad.

    Move the file named filename to the folder badtraces_dir, if the trace it
    contains it too bad, i.e. its badness is greater than rel_threshold.

    The badness is the ratio between the number of erroneous records, and the
    total number of records in the trace.

    Return True is the trace was bad and its file was moved, False otherwise.
    """
    badness = 0
    nb_records = 0

    logging.info('Checking ' + filename)

    with gzip.open(filename, 'r') as trace:
        for record_line in trace:
            cpu = float(record_line.decode('utf-8').split('\t')[1])
            nb_records += 1
            if cpu == 0.0:
                badness += 1

    if badness / nb_records > rel_threshold:
        logging.info(filename + 'was bad, moving it')
        target = os.path.join(badtraces_dir,
                              os.path.split(os.path.dirname(filename))[1])
        os.makedirs(target, exist_ok=True)
        shutil.move(filename, target)
        return True

    return False

def check_quality_and_move(input_dir, badtraces_dir, rel_threshold):
    """Check the quality of traces in a folder.

    See the docstring of check_tracequality_and_move for details.
    """
    with Pool() as workers:
        workers_func = partial(
            check_tracequality_and_move, rel_threshold=rel_threshold,
            badtraces_dir=badtraces_dir)
        moved = workers.map(workers_func,
                            glob.glob(os.path.join(input_dir, '*', '*')))

    # True = 1, False = 0, thus the sum trick
    logging.info('Moved {} traces ({:0.2f}%)'.
                 format(sum(moved), 100 * sum(moved)/len(moved)))

def main():
    """Main method of the module."""
    try:
        check_quality_and_move(sys.argv[1], sys.argv[2], float(sys.argv[3]))
    except IndexError:
        print(globals()['__doc__'])
        exit(2)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
