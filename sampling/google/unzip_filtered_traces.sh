#!/bin/bash

FILTERED_TRACES_DIR="$1"

hadoop fs -rm "$FILTERED_TRACES_DIR/_SUCCESS"

fs_ls=$(hadoop fs -ls "$FILTERED_TRACES_DIR" | grep "$FILTERED_TRACES_DIR" | rev | cut -sf1 -d" " | rev)

for file in $fs_ls; do
	hadoop fs -cat "$file" | gunzip | hadoop fs -put - "${file%.*}"
	hadoop fs -rm "$file"
done
