#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""Find matching VMs in the processed traces.

Matching VMs are VMs that exhibit matching patterns of inactivity during their
common period of records (some VM traces have fewer records than others because
they end earlier, but they all begin at the same date).

Usage:
    ./find_matching_vm.py CONVERTED_TRACES NB_MATCHING_VMS MATCHING_ERROR_THRESH
                          [NB_TRIED_VMS]

 * CONVERTED_TRACES is the directory containing the converted traces as output
    by convert_to_minutes.py
 * NB_MATCHING_VMS is the number of matching VMs to find
 * MATCHING_ERROR_THRESH is the threshold for relative matching error between
    two examined VMs (0.1, i.e. 10% error already gives good results)
 * NB_TRIED_VMS is the number of VMs with the longest traces, selected at the
    beginning, from whom matching VMs are searched; if not specified, all VMs
    are tried

First, this program selects NB_TRIED_VMS VMs among the VMs with the longest
records traces in CONVERTED_TRACES. Then, for each such parent VM, it tries to
find NB_MATCHING_VMS - 1 other longest VMs that exhibit an inactivity pattern
close enough to the parent's one, the criterium for the relative matching error
being MATCHING_ERROR_THRESH. Finally, it scans the sets of matching VMs that were
found and determines the best matching one.
"""


import glob
from itertools import combinations
import logging
from math import fsum, sqrt
import os
import sys


class VirtualMachine:
    """Represents a virtual machine in the other dataset.

     * vm_id: the ID of the VirtualMachine (int)
     * measures: measure records of the VirtualMachine (dictionary)

    measures is a dictionary that maps measures to date (in minute). measures is
    assumed to begin at date 0, and have no "holes" in its indices
    (no missing records).

    The method for the `in` operator is overriden, in order to test for inclusion
    between VMs. `vm1 in vm2` means that the last record of vm2 comes after, or
    on the same date of the last record of vm1.
    """
    def __init__(self, vm_id, measures):
        self.vm_id = vm_id
        self.measures = measures
        self.end = len(measures) - 1

    def __contains__(self, other):
        return other.end <= self.end

    def __str__(self):
        return str(self.vm_id)

# Just a dummy virtual machine for comparison
DUMMY_VIRTUALMACHINE = VirtualMachine(0, {})

class NoNestedVM(Exception):
    """Raised when no nested VM could be found.

    A nested VM, is a VM with its last record coming before, or on the same date
    of the last record of another VM.
    """
    pass

def read_traces(dirname):
    """Read VM traces from the given directory.

    Returns a generator that yields VirtualMachine class instances.
    """
    for vm_file in glob.glob(os.path.join(dirname, '*.minTrace')):
        logging.debug('Found trace file %s', vm_file)
        vm_id = int(os.path.basename(vm_file).split('.', maxsplit=1)[0][2:])
        yield VirtualMachine(vm_id, dict(read_measures(vm_file)))

def read_measures(vm_file):
    """Read measure records of the given VM.

    Return a generator of tuples (timestamp, measure) (int, float), where
    timestamp is the date of the record and measure is the recorded CPU usage.
    """
    with open(vm_file, 'r') as measures:
        for measure in measures:
            timestamp, value = measure.split(' ')[0:2]
            yield int(timestamp), float(value)

def get_matchingvms(vms, nb_vms, rel_matchingerror_thresh, parent=None):
    """Find nb_vms VirtualMachines that match the given parent VM among vms.

    Return a set of VirtualMachines.

    The next matching VirtualMachine is chosen as the one with the longest trace
    included in the parent's one, and with a relative matching error less than
    rel_matchingerror_thresh.

    If parent is None, then it selects the VirtualMachine with the longest trace.
    """
    logging.debug('Need %d more matching VMs', nb_vms)

    # We found enough matching VMs, yay! Return the empty set to end the recursion
    if nb_vms == 0:
        return set()

    # Let's not modify the global set of VMs ;)
    cur_vms = vms.copy()

    # Exits when enough matching (nested) VMs were found, or raise NoNestedVM
    # for the calling function otherwise
    while True:
        while True:
            # Will raise NoNestedVM for us
            chosen = _find_biggest_nested(cur_vms, parent)
            cur_vms -= {chosen}
            error = matching_error({chosen, parent})
            if error < rel_matchingerror_thresh:
                break

        try:
            # chosen has already been removed from cur_vms at this point
            return {chosen} | get_matchingvms(
                cur_vms, nb_vms - 1, rel_matchingerror_thresh, chosen)
        except NoNestedVM:
            # Will loop, and try with another chosen candidate until
            # _find_biggest_nested raises NoNestedVM itself
            continue

def _find_biggest_nested(vms, parent=None):
    """Find the longest VirtualMachine included in parent.

    Return the VirtualMachine whose trace has the greater length, with its
    records ending before the last record of the parent.

    If parent is None, return the longest VirtualMachine among vms.
    """
    chosen = DUMMY_VIRTUALMACHINE

    for vm in vms:
        # VMs measures start at 0, so their length is the date of their last
        # record, plus 1 (not included in the comparison because useless).
        if (parent is None or (not parent is None and vm in parent)) \
                and vm.end > chosen.end:
            chosen = vm

    # We're still with the dummy VirtualMachine...
    if chosen is DUMMY_VIRTUALMACHINE:
        raise NoNestedVM()

    return chosen

def _find_lastmatchingrecord(vms):
    """Find the last record present in every VirtualMachine of vms.

    vms are VirtualMachines where each of their traces is included into another
    one, up to the parent VM. Thus, the last matching record is simply the
    earliest last record among all VM traces.

    Used to determine the timespan on which to compute the matching error.
    """
    return min(vm.end for vm in vms)

def matching_error(vms):
    """Compute the matching error between the traces of the VMs in vms.

    The matching error is the mean of the relative matching error of each VM
    taken two at a time.
    See the docstring of _matching_error for details about the computation of
    the matching error between two VMs.
    """
    last_record = _find_lastmatchingrecord(vms)

    cumulated_badness = 0
    # Count number of combinations to compute the mean badness.
    # (len does not work on the return value of combinations)
    vm_couples = 0
    for vm1, vm2 in combinations(vms, 2):
        # We compute the matching error over the common date slice of all VMs,
        # instead of just the slice that is common to the two current VMs.
        cumulated_badness += _matching_error(vm1, vm2, last_record)
        vm_couples += 1

    return cumulated_badness / vm_couples

def _matching_error(vm1, vm2, end=None):
    """Compute the relative matching error between two traces of VirtualMachine.

    Ideally, two matching VMs have measure traces, such that the trace of one
    is the image by an affine transformation of the other. Thus, the derivative
    of the first trace, is the derivative of the second trace scaled by a
    coefficient named a.
    For each record date (before end if not None, otherwise from 0 to the last
    common record), we compute the derivative of each trace: their ratio
    gives the scaling coefficient a. We can then compute the standard deviation
    of this series of coefficients. It is rendered relative, by dividing by the
    difference between the biggest value and the lowest value of a.
    """
    # Honor given date slice bounds if specified, otherwise find the last record
    # we can compute onto.
    if end is None:
        end = _find_lastmatchingrecord([vm1, vm2])

    # Compute the scaling coefficient on each date of the timespan
    affine_coeff = {}
    for date in range(0, end):
        vm1_prime = vm1.measures[date+1] - vm1.measures[date]
        vm2_prime = vm2.measures[date+1] - vm2.measures[date]
        affine_coeff[date] = vm2_prime / vm1_prime if vm1_prime != 0 else 1
    # Compute components of the variance
    mean = fsum(affine_coeff.values()) / (end + 1)
    sqr_mean = fsum([coeff**2 for coeff in affine_coeff.values()]) / (end + 1)
    # Compute the standard deviation (i.e. sqrt(variance)), and make it relative
    return sqrt(sqr_mean - mean**2) / (max(affine_coeff) - min(affine_coeff))

def get_bestmatchingvms(vms, nb_vms, rel_matchingerror_thresh, nb_parents=None):
    """Find the best matching set of VirtualMachines.

    Return a set of nb_vms VirtualMachines such that all are matching with a
    relative error less than rel_matchingerror_thresh.

    Matching VMs, are nb_vms VMs that share a common timespan of measure
    records, and such that their matching error is less than
    rel_matchingerror_thresh. The returned set, it the one among nb_parents
    computed sets with the lowest matching error.
    """
    parents = []

    # Select the parents for the tried sets of matching VirtualMachines.
    if not nb_parents:
        parents = vms
        nb_parents = len(parents)
        logging.info('Trying with all %d VMs as parents', nb_parents)
    else:
        cur_vms = vms.copy()
        for _ in range(nb_parents):
            # let it raise NoNestedVM if it has to
            parent = _find_biggest_nested(cur_vms)
            parents.append(parent)
            cur_vms -= {parent}
        logging.info('Selected %d parents', nb_parents)

    best_matching = None
    bestmatching_error = None
    progress = 0
    for parent in parents:
        progress += 1
        logging.info('Searching with parent %d (%f%%)',
                     progress, 100 * progress / nb_parents)
        try:
            matching_vms = {parent} | get_matchingvms(
                vms - {parent}, nb_vms - 1, rel_matchingerror_thresh, parent)
        except NoNestedVM:
            # Searching failed with the given parent, let's move forward.
            # We also could find a new parent to replace it.
            logging.info('Could find matching VMs for parent %d (VM %s)',
                         progress, parent)
            continue
        error = matching_error(matching_vms)
        logging.debug('Parent %d relative matching error: %f for %d VMs',
                      progress, error, len(matching_vms))
        if bestmatching_error is None or error < bestmatching_error:
            best_matching = matching_vms
            bestmatching_error = error
            logging.info('Parent %d is the new best matching VM!', progress)

    if best_matching is None:
        raise NoNestedVM

    return best_matching

def main():
    """Main method of the module."""
    try:
        nb_parents = None
        if len(sys.argv) == 5:
            nb_parents = int(sys.argv[4])
        matching_vms = get_bestmatchingvms(
            set(read_traces(sys.argv[1])),
            int(sys.argv[2]), float(sys.argv[3]), nb_parents)
    except IndexError:
        print(globals()['__doc__'])
        exit(2)
    except NoNestedVM:
        logging.error('Couldn\'t find enough nested VMs :(')
    else:
        print('\n'.join(str(vm) for vm in matching_vms))

    logging.info('Done!')

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
