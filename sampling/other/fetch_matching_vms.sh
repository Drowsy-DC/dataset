#!/bin/bash

# Fetch matching VMs determined by "find_matching_vms.py

if [ $# -lt 2 ]; then
	echo "Usage: $0 CONVERTED_TRACES OUTPUT_DIR [MATCHING_VMS]"
	echo "CONVERTED_TRACES is the folder containing converted traces, as output \
		by convert_to_minutes.py"
	echo "OUTPUT_DIR is the output folder (must exists) matching VM traces \
	 	are copied into"
	echo "MATCHING_VMS is the input file containing the matching VMs IDs, as \
		output by find_matching_vms.py; if not present, read the list from stdin"
	exit 2
fi

if [ ! -d $1 ]; then
	echo "First argument \"$1\" is not a directory, or does not exist."
	exit 2
fi
if [ ! -d $2 ]; then
	echo "Second argument \"$2\" is not a directory, or does not exist."
	exit 2
fi
[ $# -ge 3 -a -f "$3" ] && input="$3" || input="-"

ids=$(cat $input)

for id in $ids; do
	cp "$1/VM$id.minTrace" "$2/"
done
