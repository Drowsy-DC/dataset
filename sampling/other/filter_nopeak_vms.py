#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""Filter out VMs with no peaks in the traces.

The sampling script chain seeks LLMU VMs, which exhibit strong peaks in activity.
This script moves VM traces without any peak to another folder, so the next script
"find_matching_vms.py" can be executed on a smaller set of traces.

Usage:
    ./filter_nopeak_vms.py CONVERTED_TRACES PEAK_LOW_THRESH PEAK_HIGH_THRESH
                           [NOPEAK_DIR]

 * CONVERTED_TRACES is the directory containing the converted traces as output by
    convert_to_minutes.py
 * PEAK_LOW_THRESH is the lower threshold used to detect the end of a potential peak
 * PEAK_HIGH_THRESH is the higher threshold used to detect a potential peak
 * NOPEAK_DIR is the directory the traces without peaks are moved to; if not
    specified, they are moved to a subdirectory of CONVERTED_TRACES named "nopeak"

Peak detection is the following: while iterating over the measures, the maximum
scanned value is stored. If the current measure is lower than PEAK_LOW_THRESH, and
then if the maximum value is higher than PEAK_HIGH_THRESH, then a peak was detected.
If the maximum value was not higher, then it is reset to 0 and scanning resumes.
"""


from glob import glob
import logging
import os
import shutil
import sys


def read_measures(vm_file):
    """Read measure records of the given VM.

    Return a generator of tuples (timestamp, measure) (int, float), where
    timestamp is the date of the record and measure is the recorded CPU usage.
    """
    with open(vm_file, 'r') as measures:
        for measure in measures:
            timestamp, value = measure.split(' ')[0:2]
            yield int(timestamp), float(value)

def has_peak(measures, peak_lowthresh, peak_highthresh):
    """Tell whether a list of measures exhibits a peak.

     * measures: measures to scan (dictionary)
     * peak_lowthresh: lower threshold to determine the end of a peak (float)
     * peak_highthresh: higher threshold to determine a peak (float)

    Return True is a peak was detected, False otherwise.

    measures is a dictionary that maps measures to date (in minute). The measures
    are directly compared to the thresholds, so they ought to be at the scale of
    real values.
    """
    max_cpu = 0
    for _, cpu in measures:
        max_cpu = max(max_cpu, cpu)

        if cpu < peak_lowthresh:
            if max_cpu >= peak_highthresh:
                return True

            max_cpu = 0

    return False

def get_nopeak_vms(dirname, peak_lowthresh, peak_highthresh):
    """Get the list of VM traces that contain no peak.

    Return a list of trace filenames.

    See has_peak for details on peak detection.
    """
    no_pattern = []

    for vm_file in glob(os.path.join(dirname, '*.minTrace')):
        if not has_peak(read_measures(vm_file), peak_lowthresh, peak_highthresh):
            no_pattern.append(vm_file)

    return no_pattern

def move_vms(vm_files, out_dir):
    """Move all VM trace files in vm_files to out_dir."""
    os.makedirs(out_dir, exist_ok=True)

    for vm_file in vm_files:
        logging.info("Moving out VM trace %s", vm_file)
        shutil.move(vm_file, out_dir)

def filter_dir(dirname, peak_lowthresh=0.3, peak_highthresh=0.5, out_dir=None):
    """Filter a directory to eliminate VM traces with no peaks.

    It gets the list of VM traces without any peak, and then moves them to out_dir,
    or a subdirectory named "nopeak" of the folder dirname.

    See has_peak for details on peak detection.
    """
    if out_dir is None:
        out_dir = os.path.join(dirname, "nopeak")

    filtered_out = get_nopeak_vms(dirname, peak_lowthresh, peak_highthresh)
    logging.info("%d VMs filtered out", len(filtered_out))
    move_vms(filtered_out, out_dir)

def main():
    """Main method of the module."""
    try:
        out_dir = None
        if len(sys.argv) == 5:
            out_dir = sys.argv[4]
        filter_dir(sys.argv[1], float(sys.argv[2]), float(sys.argv[3]), out_dir)
    except IndexError:
        print(globals()['__doc__'])
        exit(2)

    logging.info('Done!')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
