Other dataset
=============

This folder contains the dataset of traces of virtual machines in a public cloud.

The traces from this public cloud data-center, are measures of CPU usage, in percentage of their allocated VCPUs, over about 7 or 8 days (it varies depending on the VM). A measure is recorded every 15min, and the index is in minutes (so each index is separated by exactly 15).

One file is the trace of one VM. It contains the date index and the CPU usage measure, separated by a tabulation `\t`.
